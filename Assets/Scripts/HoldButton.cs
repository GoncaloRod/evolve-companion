using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent OnPress;
    public UnityEvent OnRelease;

    private void Awake()
    {
        OnPress = new UnityEvent();
        OnRelease = new UnityEvent();
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        OnPress.Invoke();
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        OnRelease.Invoke();
    }
}
