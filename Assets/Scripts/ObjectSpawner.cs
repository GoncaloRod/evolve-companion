using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARPlaneManager))]
[RequireComponent(typeof(ARRaycastManager))]
public class ObjectSpawner : MonoBehaviour
{
    public static GameObject ObjectToPlace;
    
    private static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    private ARPlaneManager _planeManager;
    private ARRaycastManager _raycastManager;

    private GameObject _spawnedObject;

    private bool _moving = false;

    private float _intensity = 0f;

    private Vector2[] _touchPositions = new Vector2[2];

    [Header("UI")]
    [SerializeField] private HoldButton moveButton;
    [SerializeField] private Button infoButton;
    [SerializeField] private Button closeDescriptionButton;
    [SerializeField] private GameObject descriptionPanel;
    [SerializeField] private TMP_Text descriptionText;
    
    private void Awake()
    {
        _planeManager = GetComponent<ARPlaneManager>();
        _raycastManager = GetComponent<ARRaycastManager>();
        
        moveButton.OnPress.AddListener(() => _moving = true );
        moveButton.OnRelease.AddListener(() => _moving = false );
        
        infoButton.onClick.AddListener(() =>
        {
            descriptionPanel.SetActive(true);
        });
        
        closeDescriptionButton.onClick.AddListener(() =>
        {
            descriptionPanel.SetActive(false);
        });

        Description description = ObjectToPlace.GetComponent<Description>();
        descriptionText.text = description.DescriptionText;
    }

    private void Update()
    {
        if (_spawnedObject == null)
        {
            // Try to place it
            if (RaycastFromScreenCenter())
            {
                var hitPose = s_Hits[0].pose;

                Spawn(hitPose.position, hitPose.rotation);
            }
        }
        else if (_moving)
        {
            if (RaycastFromScreenCenter())
            {
                var hitPose = s_Hits[0].pose;

                _spawnedObject.transform.position = hitPose.position;
            }

            _intensity = Mathf.MoveTowards(_intensity, _intensity + 2f * Time.deltaTime, 1f - _intensity);
            
            foreach (var trackable in _planeManager.trackables)
            {
                trackable.gameObject.GetComponent<MeshRenderer>().material.SetFloat("_Intensity", _intensity);
            }
        }
        else
        {
            _intensity = Mathf.MoveTowards(_intensity, _intensity - 2f * Time.deltaTime, _intensity);
            
            foreach (var trackable in _planeManager.trackables)
            {
                trackable.gameObject.GetComponent<MeshRenderer>().material.SetFloat("_Intensity", _intensity);
            }
        }

        if (Input.touchCount == 1)
        {
            // Rotate
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                _touchPositions[0] = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                Vector2 newPos = touch.position;

                float delta = _touchPositions[0].x - newPos.x;

                _touchPositions[0] = newPos;
                
                _spawnedObject.transform.Rotate(Vector3.up, delta * 5f * Time.deltaTime);
            }
        }
        else if (Input.touchCount == 2)
        {
            // Scale
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            
            if (touch1.phase == TouchPhase.Began)
            {
                _touchPositions[0] = touch1.position;
            }
            
            if (touch2.phase == TouchPhase.Began)
            {
                _touchPositions[1] = touch2.position;
            }
            
            else if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
            {
                Vector2 newPos1 = touch1.position;
                Vector2 newPos2 = touch2.position;

                float previousDistance = (_touchPositions[0] - _touchPositions[1]).magnitude;
                float currentDistance = (newPos1 - newPos2).magnitude;

                float delta = previousDistance - currentDistance;

                _touchPositions[0] = newPos1;
                _touchPositions[1] = newPos2;

                _spawnedObject.transform.localScale -= Vector3.one * (delta * 0.05f * Time.deltaTime);

                if (_spawnedObject.transform.localScale.x < 0.2f)
                    _spawnedObject.transform.localScale = Vector3.one * 0.2f;
                else if (_spawnedObject.transform.localScale.x > 1f)
                    _spawnedObject.transform.localScale = Vector3.one;
            }
        }
    }

    private bool RaycastFromScreenCenter()
    {
        Vector2 screenCenter = new Vector2(Screen.width / 2f, Screen.height / 2f);

        return _raycastManager.Raycast(screenCenter, s_Hits, TrackableType.PlaneWithinPolygon);
    }

    private void Spawn(Vector3 position, Quaternion rotation)
    {
        _spawnedObject = Instantiate(ObjectToPlace, position, rotation);
    }
}
