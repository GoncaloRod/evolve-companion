using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject harryPrefab;
    [SerializeField] private GameObject kerryPrefab;

    public void OnHarryButtonPressed()
    {
        ObjectSpawner.ObjectToPlace = harryPrefab;
        SceneManager.LoadScene("DisplayScene");
    }
    
    public void OnKerryButtonPressed()
    {
        ObjectSpawner.ObjectToPlace = kerryPrefab;
        SceneManager.LoadScene("DisplayScene");
    }
}
